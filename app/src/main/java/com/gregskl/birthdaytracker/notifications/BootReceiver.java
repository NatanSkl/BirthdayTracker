package com.gregskl.birthdaytracker.notifications;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gregskl.birthdaytracker.Person;
import com.gregskl.birthdaytracker.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Gregory on 29-Nov-16.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        List<Person> people = new ArrayList<>();
        try {
            File names = new File(context.getFilesDir(), "names.json");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            JSONObject object = new JSONObject(Utils.getStringFromFile(names));
            JSONArray array = (JSONArray) object.get("array");
            for (int i = 0; i < array.length(); i++) {
                JSONObject temp = array.getJSONObject(i);
                String name = String.valueOf(temp.get("name"));
                Date birthday = formatter.parse(String.valueOf(temp.get("birthday")));
                UUID id = UUID.fromString(String.valueOf(temp.get("id")));
                people.add(new Person(name, birthday, id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils.setAlarms(context, people, false);
    }
}
