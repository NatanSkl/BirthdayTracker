package com.gregskl.birthdaytracker;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Period;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by magshimim on 11/23/2016.
 */

public class BirthdayAnalyzer {

    private Date birthday;

    public BirthdayAnalyzer(Date birthday) {
        this.birthday = birthday;
    }

    public int getNextAge() {
        return getNextAgeWithDate(new Date());
    }

    public int getNextAgeWithDate(Date now) {
        if(birthday.after(now)) {
            return 0;
        }
        else {
            Period period = new Period(new DateTime(birthday), new DateTime(now));
            return period.getYears() + 1;
        }

    }

    public int getDaysUntilBirthday() {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());
        Calendar c2 = Calendar.getInstance();
        c2.setTime(birthday);
        c2.set(Calendar.YEAR, c1.get(Calendar.YEAR) + 1);
        DateTime now = new DateTime(c1.getTime());
        DateTime bday = new DateTime(c2.getTime());
        int days = Days.daysBetween(now.toLocalDate(), bday.toLocalDate()).getDays();
        if(days >= 365)
            days -= 365;
        return days;
    }

    public boolean hasBirthdayToday() {
        return getDaysUntilBirthday() == 0;
    }
}