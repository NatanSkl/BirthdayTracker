package com.gregskl.birthdaytracker;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by magshimim on 11/20/2016.
 */

public class Person implements Serializable {

    private String name;
    private Date birthday;
    private UUID id;

    public Person(String name, Date birthday, UUID id) {
        this.name = name;
        this.birthday = birthday;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
